FROM openjdk:17-slim as build
COPY . .
RUN ./gradlew build

FROM openjdk:17-slim
RUN apt-get update \
	&& apt-get -y install --no-install-recommends imagemagick webp \
	&& apt-get clean \
	&& rm -rf /var/lib/apt/lists/*
EXPOSE 8000
COPY --from=build ./build/libs/*SNAPSHOT.jar app.jar
ENTRYPOINT ["java", "-jar", "/app.jar"]