package me.neboard.files.neboardfiles;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
@EnableAutoConfiguration
public class NeboardFilesApplication {

	public static void main(String[] args) {
		SpringApplication.run(NeboardFilesApplication.class, args);
	}

}
