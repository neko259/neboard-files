package me.neboard.files.neboardfiles.tag;

import me.neboard.files.neboardfiles.domain.FileTagMeta;
import me.neboard.files.neboardfiles.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component(FindByTagStrategy.STRATEGY_PLAIN)
public class PlainMatchTagStrategy implements FindByTagStrategy {
    @Autowired
    private TagService tagService;

    @Override
    public List<FileTagMeta> findByName(String tagName, String prefix) {
        return tagService.findByName(tagName);
    }
}
