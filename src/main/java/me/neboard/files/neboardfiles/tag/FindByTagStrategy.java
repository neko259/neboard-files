package me.neboard.files.neboardfiles.tag;

import me.neboard.files.neboardfiles.domain.FileTagMeta;

import java.util.List;

public interface FindByTagStrategy {
    String STRATEGY_PLAIN = "plain";
    String PREFIX_DELIMITER = ":";

    List<FileTagMeta> findByName(String tagName, String prefix);
}
