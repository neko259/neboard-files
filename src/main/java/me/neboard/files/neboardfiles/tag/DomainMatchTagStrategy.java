package me.neboard.files.neboardfiles.tag;

import me.neboard.files.neboardfiles.domain.FileTagMeta;
import me.neboard.files.neboardfiles.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Domain file is searched starting with the lowest subdomain. E.g. if
 * searching for my.sub.neboard.me and having an image for neboard.me,
 * will be searching in order:
 *
 * my.sub.neboard.me
 * sub.neboard.me
 * neboard.me
 */
@Component("domain")
public class DomainMatchTagStrategy implements FindByTagStrategy {
    private static final String DOMAIN_JOIN_DELIMITER = ".";

    @Autowired
    private TagService tagService;

    @Override
    public List<FileTagMeta> findByName(String tagName, String prefix) {
        List<String> parts = new ArrayList<>(Arrays.asList(tagName.split("\\.")));
        List<FileTagMeta> metas = new ArrayList<>();

        while (parts.size() > 1) {
            String domain = String.join(DOMAIN_JOIN_DELIMITER, parts);
            metas = tagService.findByName(prefix + PREFIX_DELIMITER + domain);
            if (!metas.isEmpty()) {
                break;
            } else {
                parts.remove(0);
            }
        }

        return metas;
    }
}
