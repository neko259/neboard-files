package me.neboard.files.neboardfiles.service.impl;

import me.neboard.files.neboardfiles.delete.DeleteStrategy;
import me.neboard.files.neboardfiles.service.DeleteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class DeleteServiceImpl implements DeleteService {
	@Autowired
	@Qualifier("deleteStrategy")
	private DeleteStrategy deleteStrategy;

	@Override
	public void delete(String name) {
		deleteStrategy.delete(name);
	}
}
