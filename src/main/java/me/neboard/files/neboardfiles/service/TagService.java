package me.neboard.files.neboardfiles.service;

import me.neboard.files.neboardfiles.domain.FileMeta;
import me.neboard.files.neboardfiles.domain.FileTagMeta;

import java.util.List;

public interface TagService {
    void addTag(FileMeta fileMeta, String tag);
    void removeTag(FileMeta fileMeta, String tag);
    List<FileTagMeta> findByName(String tag);
}
