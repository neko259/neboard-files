package me.neboard.files.neboardfiles.service.impl;

import me.neboard.files.neboardfiles.domain.FileMeta;
import me.neboard.files.neboardfiles.domain.FileTagMeta;
import me.neboard.files.neboardfiles.repository.FileTagMetaRepository;
import me.neboard.files.neboardfiles.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TagServiceImpl implements TagService {
    @Autowired
    private FileTagMetaRepository repository;

    @Override
    public void addTag(FileMeta fileMeta, String tag) {
        FileTagMeta tagMeta = new FileTagMeta(tag, fileMeta.getName());
        repository.save(tagMeta);
    }

    @Override
    public void removeTag(FileMeta fileMeta, String tag) {
        repository.delete(repository.findByNameAndFileMetaName(tag, fileMeta.getName()));
    }

    @Override
    public List<FileTagMeta> findByName(String tag) {
        return repository.findByName(tag);
    }

}
