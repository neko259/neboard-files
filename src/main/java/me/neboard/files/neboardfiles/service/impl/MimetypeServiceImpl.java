package me.neboard.files.neboardfiles.service.impl;

import me.neboard.files.neboardfiles.exception.BusinessException;
import me.neboard.files.neboardfiles.service.MimetypeService;
import me.neboard.files.neboardfiles.service.StorageService;
import org.apache.tika.detect.DefaultDetector;
import org.apache.tika.metadata.Metadata;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

@Service
public class MimetypeServiceImpl implements MimetypeService {
    @Autowired
    private StorageService storageService;

    @Value("${url.header.timeout}")
    private Integer headerTimeout;

    @Override
    public String getMimetype(File file) throws BusinessException {
        try (InputStream is = storageService.getInputStream(file)) {
            return getMimetype(is);
        } catch (IOException e) {
            throw new BusinessException(e);
        }
    }

    @Override
    public String getMimetype(InputStream is) throws BusinessException {
        try {
            return new DefaultDetector().detect(is, new Metadata()).toString();
        } catch (IOException e) {
            throw new BusinessException(e);
        }
    }

    @Override
    public String getMimetype(URL url) throws BusinessException {
        HttpURLConnection urlConn;
        try {
            urlConn = (HttpURLConnection) url.openConnection();
            urlConn.setRequestMethod("HEAD");

            if (headerTimeout != null) {
                urlConn.setConnectTimeout(headerTimeout);
                urlConn.setReadTimeout(headerTimeout);
            }
        } catch (IOException e) {
            throw new BusinessException(e);
        }
        String contentType = urlConn.getContentType();
        return contentType == null ? null : contentType.split(";")[0];
    }

}
