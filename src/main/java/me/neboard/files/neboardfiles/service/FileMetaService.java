package me.neboard.files.neboardfiles.service;

import me.neboard.files.neboardfiles.domain.FileMeta;
import me.neboard.files.neboardfiles.exception.BusinessException;

import java.io.File;
import java.io.InputStream;
import java.util.Collection;
import java.util.Map;

public interface FileMetaService {
	Collection<FileMeta> findByHash(String hash);

	Collection<FileMeta> findByInputStream(InputStream is) throws BusinessException;

	FileMeta getOrCreate(File file) throws BusinessException;

	void save(FileMeta meta);

	void delete(String name);

	Map<String, Object> getMetadata(File file) throws BusinessException;
}
