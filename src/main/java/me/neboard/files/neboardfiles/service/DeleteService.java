package me.neboard.files.neboardfiles.service;

public interface DeleteService {
	void delete(String name);
}
