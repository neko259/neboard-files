package me.neboard.files.neboardfiles.service.impl;

import com.google.gson.Gson;
import me.neboard.files.neboardfiles.service.JsonService;
import org.springframework.stereotype.Service;

@Service
public class GsonJsonService implements JsonService {
    private final Gson gson = new Gson();

    @Override
    public String toJson(Object data) {
        return gson.toJson(data);
    }
}
