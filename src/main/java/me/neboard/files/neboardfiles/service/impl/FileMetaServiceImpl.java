package me.neboard.files.neboardfiles.service.impl;

import me.neboard.files.neboardfiles.domain.FileMeta;
import me.neboard.files.neboardfiles.exception.BusinessException;
import me.neboard.files.neboardfiles.exception.MetaNotFoundException;
import me.neboard.files.neboardfiles.metadata.MetadataRetrievalStrategy;
import me.neboard.files.neboardfiles.repository.FileMetaRepository;
import me.neboard.files.neboardfiles.service.FileMetaService;
import me.neboard.files.neboardfiles.service.HashService;
import me.neboard.files.neboardfiles.service.MimetypeService;
import org.apache.tika.detect.DefaultDetector;
import org.apache.tika.metadata.Metadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Service
public class FileMetaServiceImpl implements FileMetaService {
    private static final Logger log = LoggerFactory.getLogger(FileMetaService.class);

    @Autowired
    private FileMetaRepository repository;

    @Autowired
    private HashService hashService;

    @Autowired
    private Collection<MetadataRetrievalStrategy> metadataStrategies;

    @Autowired
    private MimetypeService mimetypeService;

    @Override
    public Collection<FileMeta> findByHash(String hash) {
        return repository.findByHash(hash);
    }

    @Override
    public Collection<FileMeta> findByInputStream(InputStream is) throws BusinessException {
        return findByHash(hashService.getHash(is));
    }

    @Override
    public FileMeta getOrCreate(File file) throws BusinessException {
        FileMeta meta = findByName(file.getName());
        if (meta == null && file.exists()) {
            meta = fromFile(file);
            save(meta);
            log.warn(String.format("Generated meta for %s", meta));
        } else if (!file.exists()) {
            throw new MetaNotFoundException("File not found");
        }
        return meta;
    }

    @Override
    public void save(FileMeta meta) {
        repository.save(meta);
    }

    @Override
    public void delete(String name) {
        repository.deleteById(name);
    }

    private FileMeta fromFile(File file) throws BusinessException {
        return new FileMeta(file.getName(), hashService.getFileHash(file), mimetypeService.getMimetype(file));
    }

    @Override
    public Map<String, Object> getMetadata(File file) throws BusinessException {
        Map<String, Object> metadata = new HashMap<>();

        FileMeta fileMeta = getOrCreate(file);
        if (fileMeta != null) {
            String mimetype = fileMeta.getMimetype();
            for (MetadataRetrievalStrategy strategy : metadataStrategies) {
                if (strategy.handles(mimetype)) {
                    metadata.putAll(strategy.getMetadata(fileMeta, file));
                }
            }
        }

        return metadata;
    }

    private FileMeta findByName(String name) {
        return repository.findById(name).orElse(null);
    }


}
