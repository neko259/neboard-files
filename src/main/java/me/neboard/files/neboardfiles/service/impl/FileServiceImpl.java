package me.neboard.files.neboardfiles.service.impl;

import me.neboard.files.neboardfiles.creation.CreationOptions;
import me.neboard.files.neboardfiles.domain.FileMeta;
import me.neboard.files.neboardfiles.domain.FileTagMeta;
import me.neboard.files.neboardfiles.exception.BusinessException;
import me.neboard.files.neboardfiles.model.FileInfo;
import me.neboard.files.neboardfiles.model.ResponseFileInfo;
import me.neboard.files.neboardfiles.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@Service
public class FileServiceImpl implements FileService {
    private static final Logger log = LoggerFactory.getLogger(FileService.class);

    @Autowired
    private FileMetaService metaService;

    @Autowired
    private StorageService storageService;

    @Autowired
    private SaveService saveService;

    @Autowired
    private DeleteService deleteService;

    @Autowired
    private DeduplicationService deduplicationService;

    @Autowired
    private TagService tagService;

    @Autowired
    private TagResolver tagResolver;

    @Override
    public FileMeta getFileMeta(String name) throws BusinessException {
        return getFileMeta(storageService.getFile(name));
    }

    @Override
    public boolean fileExists(String name) {
        return storageService.getFile(name).exists();
    }

    @Override
    public void delete(String name) {
        deleteService.delete(name);
        log.debug(String.format("Invoked delete for [%s]", name));
    }

    /**
     * Create file or get one if a duplicate exists.
     */
    @Override
    public FileInfo createFile(MultipartFile originalFile, boolean isImport) throws BusinessException {
        String newFilename = deduplicationService.findDuplicate(originalFile);

        FileMeta meta;
        if (newFilename == null || isImport) {
            CreationOptions options = new CreationOptions();
            if (isImport) {
                options.add(CreationOptions.OPTION_CREATION_TYPE, "import");
            }
            meta = saveService.saveFile(originalFile, options);
        } else {
            meta = metaService.getOrCreate(storageService.getFile(newFilename));
        }

        return new FileInfo(meta);
    }

    @Override
    public FileInfo createFile(Map<String, Object> options) throws BusinessException {
        // TODO Search for duplicates in this case too. Maybe even get rid of the
        // thumb_name practice and save thumb as separate images whose names
        // will be saved by the caller?
        return new FileInfo(saveService.saveFile(null, new CreationOptions(options)));
    }

    @Override
    public Map<String, Object> getMetadata(String name) throws BusinessException {
        return metaService.getMetadata(storageService.getFile(name));
    }

    @Override
    public void regenerateMetadata() throws BusinessException {
        Collection<File> files = storageService.getAllFiles();
        for (File file : files) {
            getFileMeta(file);
        }
    }

    @Override
    public ResponseFileInfo getResponseFileInfo(String name) throws BusinessException, IOException {
        File file = getFile(name);
        FileMeta meta = metaService.getOrCreate(file);

        return getResponseFileInfo(file, meta);
    }

    @Override
    public ResponseFileInfo getResponseFileInfo(File file, FileMeta meta) throws BusinessException, IOException {
        return new ResponseFileInfo(new InputStreamResource(storageService.getInputStream(file)),
                file.length(),
                MediaType.parseMediaType(meta.getMimetype()), meta.getHash());
    }

    @Override
    public File getFile(String name) {
        return storageService.getFile(name);
    }

    @Override
    public long getCount() throws BusinessException {
        return storageService.getAllFiles().size();
    }

    @Override
    public void addTag(String filename, String tag) throws BusinessException {
        tagService.addTag(getFileMeta(filename), tag);
    }

    @Override
    public List<FileMeta> findByTag(String tag) throws BusinessException {
        List<FileMeta> metas = new ArrayList<>();
        for (FileTagMeta tagMeta : tagResolver.findByNameAndPrefix(tag)) {
            FileMeta meta = getFileMeta(tagMeta.getFileMetaName());
            if (meta != null) {
                metas.add(meta);
            }
        }
        return metas;
    }

    private FileMeta getFileMeta(File file) throws BusinessException {
        return metaService.getOrCreate(file);
    }

}
