package me.neboard.files.neboardfiles.service;

public interface DownloadService {
    void download(String url);
}
