package me.neboard.files.neboardfiles.service.impl;

import me.neboard.files.neboardfiles.creation.CreationOptions;
import me.neboard.files.neboardfiles.creation.FileCreationStrategy;
import me.neboard.files.neboardfiles.domain.FileMeta;
import me.neboard.files.neboardfiles.exception.BusinessException;
import me.neboard.files.neboardfiles.service.SaveService;
import me.neboard.files.neboardfiles.service.StorageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;

@Service
public class SaveServiceImpl implements SaveService {
	private static final Logger log = LoggerFactory.getLogger(SaveService.class);

	@Autowired
	private Collection<FileCreationStrategy> creationStrategies;

	@Autowired
	private FileMetaServiceImpl metaService;

	@Autowired
	private StorageService storageService;

	@Override
	public FileMeta saveFile(MultipartFile originalFile, CreationOptions options) throws BusinessException {
		try {
			String originalFilename = null;
			InputStream is = null;
			if (originalFile != null) {
				originalFilename = originalFile.getOriginalFilename();
				is = originalFile.getInputStream();
			}
			return saveFile(originalFilename, is, options);
		} catch (IOException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public FileMeta saveFile(String originalFilename, InputStream inputStream, CreationOptions options) throws BusinessException {
		FileMeta meta = null;
		for (FileCreationStrategy strategy : creationStrategies) {
			if (strategy.handles(originalFilename, options)) {
				String filename = strategy.saveFile(originalFilename, inputStream, options);

				meta = metaService.getOrCreate(storageService.getFile(filename));

				log.info(String.format("Saved file %s", meta));

				break;
			}
		}

		if (meta == null) {
			throw new BusinessException("No creation strategy for the options");
		}

		return meta;
	}
}
