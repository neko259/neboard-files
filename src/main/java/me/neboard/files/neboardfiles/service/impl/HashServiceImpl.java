package me.neboard.files.neboardfiles.service.impl;

import me.neboard.files.neboardfiles.exception.BusinessException;
import me.neboard.files.neboardfiles.service.HashService;
import me.neboard.files.neboardfiles.service.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import javax.xml.bind.DatatypeConverter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

@Service
public class HashServiceImpl implements HashService {
    @Autowired
    private StorageService storageService;

    @Override
    public String getFileHash(File file) throws BusinessException {
        try (InputStream is = storageService.getInputStream(file)) {
            return getHash(is);
        } catch (IOException e) {
            throw new BusinessException(e);
        }
    }

    @Override
    public String getHash(InputStream is) throws BusinessException {
        try {
            return DatatypeConverter.printHexBinary(DigestUtils.md5Digest(is));
        } catch (IOException e) {
            throw new BusinessException(e);
        }
    }
}
