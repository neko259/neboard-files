package me.neboard.files.neboardfiles.service;

import me.neboard.files.neboardfiles.exception.BusinessException;

import java.io.File;
import java.io.InputStream;
import java.net.URL;

public interface MimetypeService {
    String getMimetype(File file) throws BusinessException;
    String getMimetype(InputStream is) throws BusinessException;
    String getMimetype(URL url) throws BusinessException;
}
