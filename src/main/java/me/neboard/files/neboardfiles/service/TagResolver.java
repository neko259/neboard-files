package me.neboard.files.neboardfiles.service;

import me.neboard.files.neboardfiles.domain.FileTagMeta;

import java.util.List;

public interface TagResolver {
    List<FileTagMeta> findByNameAndPrefix(String tag);
}
