package me.neboard.files.neboardfiles.service.impl;

import me.neboard.files.neboardfiles.amqp.Message;
import me.neboard.files.neboardfiles.amqp.RabbitConfiguration;
import me.neboard.files.neboardfiles.creation.CreationOptions;
import me.neboard.files.neboardfiles.domain.FileMeta;
import me.neboard.files.neboardfiles.exception.BusinessException;
import me.neboard.files.neboardfiles.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;

@Service
public class DownloadServiceImpl implements DownloadService {
    private static final Logger log = LoggerFactory.getLogger(DownloadService.class);

    @Value("${url.download.timeout}")
    private Integer downloadTimeout;

    @Value("${url.header.timeout}")
    private Integer headerTimeout;

    @Autowired
    private MimetypeService mimetypeService;

    @Autowired
    private SaveService saveService;

    @Autowired
    private StorageService storageService;

    @Autowired
    private AmqpTemplate amqpTemplate;

    @Autowired
    private JsonService jsonService;

    @Autowired
    private RabbitConfiguration rabbitConfiguration;

    @Override
    @Async
    public void download(String url) {
        File result = null;

        String status = Message.STATUS_FAILED;
        String filename = null;

        try {
            URL urlObj = new URL(url);
            if (MediaType.TEXT_HTML_VALUE.equals(mimetypeService.getMimetype(urlObj))) {
                // fail
            } else if (ResourceUtils.URL_PROTOCOL_FILE.equals(urlObj.getProtocol())) {
                // fail
            } else {
                try {
                    result = download(urlObj);
                } catch (Exception e) {
                    // Just treat it as an url if download failed for
                    // any reason
                }

                if (result != null) {
                    if (!shouldKeepDownload(result)) {
                        result.delete();
                        // fail
                    } else {
                        filename = result.getName();
                        status = Message.STATUS_COMPLETED;
                    }
                }

            }
        } catch (MalformedURLException | BusinessException e) {
            // fail
        }

        Map<String, Object> data = new HashMap<>();
        data.put("url", url);
        data.put("filename", filename);

        amqpTemplate.convertAndSend(rabbitConfiguration.getTopicExchangeName(),
                "neboard.files", jsonService.toJson(new Message("download",
                        status, data)));
    }

    private void setConnectionTimeout(URLConnection urlConn, Integer timeout) {
        if (timeout != null) {
            urlConn.setConnectTimeout(timeout);
            urlConn.setReadTimeout(timeout);
        }
    }

    private File download(URL url) throws BusinessException {
        try {
            URLConnection urlConn = url.openConnection();

            setConnectionTimeout(urlConn, downloadTimeout);

            String originalName = getFileName(url);

            File file;
            try (InputStream is = urlConn.getInputStream()) {
                FileMeta meta = saveService.saveFile(originalName, is, new CreationOptions());
                file = storageService.getFile(meta.getName());
            } catch (IOException e) {
                throw new BusinessException(e);
            }

            return file;
        } catch (IOException e) {
            throw new BusinessException(e);
        }
    }

    private boolean shouldKeepDownload(File download) {
        String contentType = null;
        try (InputStream is = new BufferedInputStream(new FileInputStream(download))) {
            contentType = mimetypeService.getMimetype(is);
        } catch (IOException | BusinessException e) {
            // We won't check mimetype if something failed
        }

        return !MediaType.TEXT_HTML_VALUE.equals(contentType);
    }

    public String getFileName(URL url) throws BusinessException {
        URLConnection urlConn;
        try {
            urlConn = url.openConnection();

            setConnectionTimeout(urlConn, headerTimeout);
        } catch (IOException e) {
            throw new BusinessException(e);
        }

        String filename = null;
        String raw = urlConn.getHeaderField("Content-Disposition");
        if(raw != null && raw.contains("=")) {
            filename = raw.replaceFirst("(?i)^.*filename=\"?([^\"]+)\"?.*$", "$1");
        }

        if (filename == null) {
            String[] urlParts = url.getPath().split("/");
            filename = urlParts[urlParts.length - 1];
        }

        return filename;
    }

}
