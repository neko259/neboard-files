package me.neboard.files.neboardfiles.service.impl;

import me.neboard.files.neboardfiles.domain.FileTagMeta;
import me.neboard.files.neboardfiles.service.TagResolver;
import me.neboard.files.neboardfiles.tag.FindByTagStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class StrategyBasedTagResolver implements TagResolver {
    private static final Pattern REGEX_TAG = Pattern.compile("((?<prefix>.+):)?(?<name>.+)");

    @Autowired
    private Map<String, FindByTagStrategy> strategies;

    @Override
    public List<FileTagMeta> findByNameAndPrefix(String tag) {
        Matcher match = REGEX_TAG.matcher(tag);

        if (match.matches()) {
            String prefix = match.group("prefix");
            String tagName = match.group("name");

            FindByTagStrategy strategy = null;

            if (prefix != null) {
                strategy = strategies.get(prefix);
            }

            if (strategy == null) {
                strategy = strategies.get(FindByTagStrategy.STRATEGY_PLAIN);
            }

            return strategy.findByName(tagName, prefix);
        } else {
            throw new UnsupportedOperationException("This tag syntax is not supported");
        }
    }
}
