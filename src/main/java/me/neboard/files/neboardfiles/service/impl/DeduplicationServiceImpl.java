package me.neboard.files.neboardfiles.service.impl;

import me.neboard.files.neboardfiles.domain.FileMeta;
import me.neboard.files.neboardfiles.exception.BusinessException;
import me.neboard.files.neboardfiles.service.DeduplicationService;
import me.neboard.files.neboardfiles.service.FileMetaService;
import me.neboard.files.neboardfiles.service.StorageService;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Collections;

@Service
public class DeduplicationServiceImpl implements DeduplicationService {
	@Autowired
	private FileMetaService metaService;

	@Autowired
	private StorageService storageService;

	@Override
	public String findDuplicate(MultipartFile file) {
		String duplicate = null;
		Collection<FileMeta> files = Collections.emptyList();
		try (InputStream is = file.getInputStream()) {
			files = metaService.findByInputStream(is);
		} catch (BusinessException | IOException e) {
			// Assume no duplicate was found
		}

		// TODO Different strategies, e.g. search only by hash
		for (FileMeta fileMeta : files) {
			try (InputStream is = file.getInputStream()) {
				String filename = fileMeta.getName();
				if (compare(is, filename)) {
					duplicate = filename;
					break;
				}
			} catch (IOException e) {
				// Assume no duplicate was found
			}
		}

		return duplicate;
	}

	private boolean compare(InputStream is, String otherFilename) {
		try (InputStream fis1 = new FileInputStream(storageService.getFile(otherFilename))) {
			if (IOUtils.contentEquals(is, fis1)) {
				return true;
			}
		} catch (IOException e) {
			// If we cannot read an old file for some reason, treat it as
			// an absent duplicate.
		}
		return false;
	}

}
