package me.neboard.files.neboardfiles.service;

import me.neboard.files.neboardfiles.domain.FileMeta;
import me.neboard.files.neboardfiles.exception.BusinessException;
import me.neboard.files.neboardfiles.model.FileInfo;
import me.neboard.files.neboardfiles.model.ResponseFileInfo;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface FileService {
	FileMeta getFileMeta(String name) throws BusinessException;

	boolean fileExists(String name);

	void delete(String name);

	FileInfo createFile(MultipartFile originalFile, boolean isImport) throws BusinessException;

	FileInfo createFile(Map<String, Object> options) throws BusinessException;

	Map<String, Object> getMetadata(String name) throws BusinessException;

	void regenerateMetadata() throws BusinessException;

	ResponseFileInfo getResponseFileInfo(String name) throws BusinessException, IOException;
	ResponseFileInfo getResponseFileInfo(File file, FileMeta meta) throws BusinessException, IOException;

	File getFile(String name);

	long getCount() throws BusinessException;

	void addTag(String filename, String tag) throws BusinessException;
	List<FileMeta> findByTag(String tag) throws BusinessException;
}
