package me.neboard.files.neboardfiles.service;

public interface JsonService {
    String toJson(Object data);
}
