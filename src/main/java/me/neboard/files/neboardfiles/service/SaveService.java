package me.neboard.files.neboardfiles.service;

import me.neboard.files.neboardfiles.creation.CreationOptions;
import me.neboard.files.neboardfiles.domain.FileMeta;
import me.neboard.files.neboardfiles.exception.BusinessException;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;

public interface SaveService {
	FileMeta saveFile(MultipartFile originalFile, CreationOptions options) throws BusinessException;
	FileMeta saveFile(String originalFilename, InputStream inputStream, CreationOptions options) throws BusinessException;
}
