package me.neboard.files.neboardfiles.service;

import org.springframework.web.multipart.MultipartFile;

public interface DeduplicationService {
	String findDuplicate(MultipartFile file);
}
