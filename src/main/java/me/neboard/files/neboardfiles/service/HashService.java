package me.neboard.files.neboardfiles.service;

import me.neboard.files.neboardfiles.exception.BusinessException;

import java.io.File;
import java.io.InputStream;

public interface HashService {
	String getFileHash(File file) throws BusinessException;

	String getHash(InputStream is) throws BusinessException;
}
