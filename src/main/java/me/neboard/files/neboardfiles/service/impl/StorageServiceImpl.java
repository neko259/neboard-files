package me.neboard.files.neboardfiles.service.impl;

import me.neboard.files.neboardfiles.exception.BusinessException;
import me.neboard.files.neboardfiles.location.FileLocationStrategy;
import me.neboard.files.neboardfiles.service.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.stream.Collectors;

@Service
public class StorageServiceImpl implements StorageService {
    @Value("${dir.files}")
    private String filesDirectory;

    @Autowired
    @Qualifier("fileLocationStrategy")
    private FileLocationStrategy fileLocationStrategy;

    @Override
    public File getFile(String name) {
        return new File(filesDirectory, fileLocationStrategy.getPath(name));
    }

    @Override
    public Collection<File> getAllFiles() throws BusinessException {
        try {
            return Files.walk(Paths.get(filesDirectory))
                    .filter(Files::isRegularFile)
                    .map(Path::toFile)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new BusinessException(e);
        }
    }

    @Override
    public InputStream getInputStream(File file) throws IOException {
        return new BufferedInputStream(Files.newInputStream(file.toPath()));
    }

}
