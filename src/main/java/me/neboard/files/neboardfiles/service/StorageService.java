package me.neboard.files.neboardfiles.service;

import me.neboard.files.neboardfiles.exception.BusinessException;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;

public interface StorageService {
	File getFile(String name);

    Collection<File> getAllFiles() throws BusinessException;

    InputStream getInputStream(File file) throws IOException;
}
