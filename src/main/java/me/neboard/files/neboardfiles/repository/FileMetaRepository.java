package me.neboard.files.neboardfiles.repository;

import me.neboard.files.neboardfiles.domain.FileMeta;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;

public interface FileMetaRepository extends JpaRepository<FileMeta, String> {
    Collection<FileMeta> findByHash(String hash);
}
