package me.neboard.files.neboardfiles.repository;

import me.neboard.files.neboardfiles.domain.FileTagMeta;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FileTagMetaRepository extends JpaRepository<FileTagMeta, Long> {
    List<FileTagMeta> findByName(String name);
    FileTagMeta findByNameAndFileMetaName(String name, String fileMetaName);
}
