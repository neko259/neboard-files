package me.neboard.files.neboardfiles.delete;

import me.neboard.files.neboardfiles.strategy.Strategy;

public interface DeleteStrategy extends Strategy {
    void delete(String name);
}
