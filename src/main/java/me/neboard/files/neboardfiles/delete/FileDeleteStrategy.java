package me.neboard.files.neboardfiles.delete;

import me.neboard.files.neboardfiles.service.FileMetaService;
import me.neboard.files.neboardfiles.service.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class FileDeleteStrategy implements DeleteStrategy {
    private static final String TYPE = "delete";

    @Autowired
    private StorageService storageService;

    @Autowired
    private FileMetaService metaService;

    @Override
    public void delete(String name) {
        storageService.getFile(name).delete();
        metaService.delete(name);
    }

    @Override
    public String getType() {
        return TYPE;
    }
}
