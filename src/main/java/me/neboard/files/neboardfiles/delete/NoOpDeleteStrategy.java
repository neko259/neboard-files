package me.neboard.files.neboardfiles.delete;

import org.springframework.stereotype.Component;

@Component
public class NoOpDeleteStrategy implements DeleteStrategy {
    private static final String TYPE = "noop";

    @Override
    public void delete(String name) {
        // This strategy does not delete anything at all
    }

    @Override
    public String getType() {
        return TYPE;
    }
}
