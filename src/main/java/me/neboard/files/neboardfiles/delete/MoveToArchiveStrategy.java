package me.neboard.files.neboardfiles.delete;

import me.neboard.files.neboardfiles.service.FileMetaService;
import me.neboard.files.neboardfiles.service.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;

@Component
public class MoveToArchiveStrategy implements DeleteStrategy {
    private static final String TYPE = "move";

    @Value("${dir.archive}")
    private String archiveDir;

    @Autowired
    private StorageService storageService;

    @Autowired
    private FileMetaService metaService;

    @Override
    public void delete(String name) {
        File removalDir = new File(archiveDir);
        if (!removalDir.exists()) {
            removalDir.mkdirs();
        }

        File file = storageService.getFile(name);
        file.renameTo(new File(removalDir, name));

        metaService.delete(name);
    }

    @Override
    public String getType() {
        return TYPE;
    }
}
