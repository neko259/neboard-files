package me.neboard.files.neboardfiles.delete;

import me.neboard.files.neboardfiles.strategy.AbstractStrategyConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DeleteConfiguration extends AbstractStrategyConfiguration<DeleteStrategy> {
    @Value("${strategy.delete}")
    private String strategyType;

    @Bean
    public DeleteStrategy deleteStrategy() {
        return getStrategy();
    }

    @Override
    protected String getStrategyType() {
        return strategyType;
    }
}
