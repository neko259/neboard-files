package me.neboard.files.neboardfiles.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FileMeta {
    @Id
    @Column
    private String name;

    @Column
    private String hash;

    @Column
    private String mimetype;

    @Override
    public String toString() {
        return String.format("[%s] of type [%s] with hash [%s]",
                getName(), getMimetype(), getHash());
    }
}
