package me.neboard.files.neboardfiles.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
public class FileTagMeta {
    @Id
    @Column
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    @Column
    private String name;

    @Column
    private String fileMetaName;

    public FileTagMeta(String name, String fileMetaName) {
        this.name = name;
        this.fileMetaName = fileMetaName;
    }
}
