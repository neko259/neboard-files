package me.neboard.files.neboardfiles.model;

import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;

public class ResponseFileInfo {
    private Resource resource;
    private long size;
    private MediaType mediaType;
    private String hash;

    public ResponseFileInfo(Resource resource, long size, MediaType mediaType, String hash) {
        this.resource = resource;
        this.size = size;
        this.mediaType = mediaType;
        this.hash = hash;
    }

    public Resource getResource() {
        return resource;
    }

    public long getSize() {
        return size;
    }

    public MediaType getMediaType() {
        return mediaType;
    }

    public String getHash() {
        return hash;
    }
}
