package me.neboard.files.neboardfiles.model;

import me.neboard.files.neboardfiles.domain.FileMeta;

public class FileInfo {
    private String name;
    private String mimetype;

    public FileInfo(String name, String mimetype) {
        this.name = name;
        this.mimetype = mimetype;
    }

    public FileInfo(FileMeta meta) {
        this(meta.getName(), meta.getMimetype());
    }

    public String getName() {
        return name;
    }

    public String getMimetype() {
        return mimetype;
    }

}
