package me.neboard.files.neboardfiles.metadata;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.MetadataException;
import com.drew.metadata.exif.ExifIFD0Directory;
import me.neboard.files.neboardfiles.domain.FileMeta;
import me.neboard.files.neboardfiles.exception.BusinessException;
import org.im4java.core.IM4JavaException;
import org.im4java.core.Info;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Component
public class ImageMetadataStrategy implements MetadataRetrievalStrategy {

    private static final int EXIF_ORIENTATION_DEFAULT = 1;

    private static final String ATTR_WIDTH = "width";
    private static final String ATTR_HEIGHT = "height";
    private static final String ATTR_ORIENTATION = "orientation";

    private static final Logger logger = LoggerFactory.getLogger(ImageMetadataStrategy.class);

    private static final Set<String> IMAGE_MIMETYPES = new HashSet<String>() {{
        add(MediaType.IMAGE_JPEG_VALUE);
        add(MediaType.IMAGE_PNG_VALUE);
        add(MediaType.IMAGE_GIF_VALUE);
        add("image/webp");
        add("image/bmp");
    }};

    @Override
    public boolean handles(String mimetype) {
        return IMAGE_MIMETYPES.contains(mimetype);
    }

    @Override
    public Map<String, Object> getMetadata(FileMeta fileMeta, File file) throws BusinessException {
        Map<String, Object> metadata = new HashMap<>();
        try {
            Info imageInfo = new Info(file
                    .getAbsolutePath(), true);
            metadata.put(ATTR_WIDTH, imageInfo.getImageWidth());
            metadata.put(ATTR_HEIGHT, imageInfo.getImageHeight());
        } catch (IM4JavaException e) {
            logger.error("Error getting image size", e);
        }

        int exifOrientation = EXIF_ORIENTATION_DEFAULT;
        try {
            Metadata meta = ImageMetadataReader.readMetadata(file);
            Directory dir = meta.getFirstDirectoryOfType(ExifIFD0Directory.class);
            if (dir != null) {
                exifOrientation = dir.getInt(ExifIFD0Directory.TAG_ORIENTATION);
            }
        } catch (MetadataException | ImageProcessingException | IOException e)  {
            logger.warn("Error getting image orientation", e);
            // Assume this is a default orientation
        }
        metadata.put(ATTR_ORIENTATION, exifOrientation);

        return metadata;
    }
}
