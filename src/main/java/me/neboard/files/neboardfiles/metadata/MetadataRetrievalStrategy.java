package me.neboard.files.neboardfiles.metadata;

import me.neboard.files.neboardfiles.domain.FileMeta;
import me.neboard.files.neboardfiles.exception.BusinessException;

import java.io.File;
import java.util.Map;

public interface MetadataRetrievalStrategy {
    boolean handles(String mimetype);
    Map<String, Object> getMetadata(FileMeta fileMeta, File file) throws BusinessException;
}
