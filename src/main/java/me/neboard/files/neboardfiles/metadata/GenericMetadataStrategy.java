package me.neboard.files.neboardfiles.metadata;

import me.neboard.files.neboardfiles.domain.FileMeta;
import me.neboard.files.neboardfiles.exception.BusinessException;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

@Component
public class GenericMetadataStrategy implements MetadataRetrievalStrategy {

    private static final String ATTR_HASH = "hash";
    private static final String ATTR_MIMETYPE = "mimetype";
    private static final String ATTR_SIZE = "size";

    @Override
    public boolean handles(String mimetype) {
        return true;
    }

    @Override
    public Map<String, Object> getMetadata(FileMeta fileMeta, File file) throws BusinessException {
        Map<String, Object> metadata = null;

        if (fileMeta != null) {
            metadata = new HashMap<>();
            metadata.put(ATTR_HASH, fileMeta.getHash());
            metadata.put(ATTR_MIMETYPE, fileMeta.getMimetype());
            metadata.put(ATTR_SIZE, file.length());
        }

        return metadata;
    }
}
