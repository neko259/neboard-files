package me.neboard.files.neboardfiles.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class MetaNotFoundException extends BusinessException {
    public MetaNotFoundException() {
        super();
    }

    public MetaNotFoundException(Throwable cause) {
        super(cause);
    }

    public MetaNotFoundException(String message) {
        super(message);
    }
}
