package me.neboard.files.neboardfiles.exception;

public class BusinessException extends Exception {
    public BusinessException() {
    }

    public BusinessException(Throwable cause) {
        super(cause);
    }

    public BusinessException(String message) {
        super(message);
    }
}
