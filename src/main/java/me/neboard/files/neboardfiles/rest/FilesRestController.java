package me.neboard.files.neboardfiles.rest;

import me.neboard.files.neboardfiles.domain.FileMeta;
import me.neboard.files.neboardfiles.exception.BusinessException;
import me.neboard.files.neboardfiles.model.FileInfo;
import me.neboard.files.neboardfiles.model.ResponseFileInfo;
import me.neboard.files.neboardfiles.service.DownloadService;
import me.neboard.files.neboardfiles.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Produces;
import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
public class FilesRestController {
    private static final String URL_GET = "/{name}";
    private static final String URL_METADATA = URL_GET + "/metadata";
    private static final String URL_CREATE = "/";
    private static final String URL_EXISTS = URL_GET;
    private static final String URL_DELETE = URL_GET;
    private static final String URL_REGENERATE = "/service/regenerate";
    private static final String URL_HEALTH = "/health";
    private static final String URL_STATS = "/stats";
    private static final String URL_DOWNLOAD = "/download";

    private static final String URL_ADD_TAG = "/tag/{name}/**";
    private static final String URL_GET_BY_TAG = "/tag/**";

    private static final Duration RESPONSE_MAX_AGE = Duration.ofDays(30);

    @Autowired
    private FileService fileService;

    @Autowired
    private DownloadService downloadService;

    @GetMapping(URL_GET)
    public ResponseEntity<Resource> getFile(@PathVariable String name, WebRequest request) throws BusinessException {
        ResponseEntity<Resource> response = null;

        FileMeta meta = null;
        meta = fileService.getFileMeta(name);
        if (meta != null && request.checkNotModified(meta.getHash())) {
            response = ResponseEntity
                    .status(HttpStatus.NOT_MODIFIED)
                    .cacheControl(CacheControl.maxAge(RESPONSE_MAX_AGE))
                    .build();
        }

        if (response == null) {
            try {
                File file = fileService.getFile(name);
                ResponseFileInfo sfi = fileService.getResponseFileInfo(file, meta);
                response = ResponseEntity
                        .ok()
                        .eTag(sfi.getHash())
                        .contentLength(sfi.getSize())
                        .contentType(sfi.getMediaType())
                        .cacheControl(CacheControl.maxAge(RESPONSE_MAX_AGE))
                        .body(sfi.getResource());
            } catch (IOException e) {
                response = ResponseEntity.notFound().build();
            }
        }
        return response;
    }

    @GetMapping(URL_METADATA)
    @Produces(MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String, Object>> getMetadata(@PathVariable String name) throws BusinessException {
        ResponseEntity<Map<String, Object>> response;
        Map<String, Object> metadata = fileService.getMetadata(name);
        return ResponseEntity.ok(metadata);
    }

    @RequestMapping(value = URL_EXISTS, method = RequestMethod.HEAD)
    public ResponseEntity<Void> fileExists(@PathVariable String name) {
        ResponseEntity<Void> response;
        if (!fileService.fileExists(name)) {
            response = ResponseEntity.notFound().build();
        } else {
            response = ResponseEntity.noContent().build();
        }
        return response;
    }

    @DeleteMapping(URL_DELETE)
    public void delete(@PathVariable String name) {
        fileService.delete(name);
    }

    @PostMapping(value = URL_CREATE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<FileInfo> createFile(@RequestParam("file") MultipartFile file,
                                               @RequestParam(value = "import", required = false) boolean isImport) throws BusinessException {
        return ResponseEntity.ok(fileService.createFile(file, isImport));
    }

    @PostMapping(value = URL_CREATE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FileInfo> createFile(@RequestBody(required = false) Map<String, Object> options) throws BusinessException {
        return ResponseEntity.ok(fileService.createFile(options));
    }

    @PostMapping(value = URL_REGENERATE)
    public ResponseEntity<Void> regenerate() throws BusinessException {
        fileService.regenerateMetadata();
        return ResponseEntity.noContent().build();
    }

    @GetMapping(URL_HEALTH)
    public ResponseEntity<Void> health() {
        return ResponseEntity.noContent().build();
    }

    @GetMapping(URL_STATS)
    public ResponseEntity<Map<String, Object>> stats() throws BusinessException {
        Map<String, Object> stats = new HashMap<>();
        stats.put("filesCount", fileService.getCount());
        return ResponseEntity.ok(stats);
    }

    @PostMapping(value = URL_DOWNLOAD)
    public ResponseEntity<Void> download(@RequestParam("url") String url) throws BusinessException {
        downloadService.download(url);
        return ResponseEntity.accepted().build();
    }

    @PostMapping(URL_ADD_TAG)
    public ResponseEntity<Void> addMeta(@PathVariable("name") String filename,
                                        HttpServletRequest request) throws BusinessException {
        String tag = request.getRequestURL().toString().split("/tag/" + filename + "/")[1];
        fileService.addTag(filename, tag);
        return ResponseEntity.ok().build();
    }

    @GetMapping(URL_GET_BY_TAG)
    public ResponseEntity<List<String>> getByTag(HttpServletRequest request) throws BusinessException {
        String tag = request.getRequestURL().toString().split("/tag/")[1];
        List<FileMeta> metas = fileService.findByTag(tag);
        return ResponseEntity.ok(metas.stream().map(FileMeta::getName).collect(Collectors.toList()));
    }

}
