package me.neboard.files.neboardfiles.creation;

import me.neboard.files.neboardfiles.exception.BusinessException;
import me.neboard.files.neboardfiles.service.StorageService;
import org.im4java.core.ConvertCmd;
import org.im4java.core.IM4JavaException;
import org.im4java.core.IMOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

@Component
public class ImageThumbStrategy extends AbstractOptionStrategy {
    private static final String OPTION_PREFIX = "thumbPrefix";
    private static final String OPTION_THUMB_WIDTH = "thumbWidth";
    private static final String OPTION_THUMB_HEIGHT = "thumbHeight";

    private static final String TYPE_THUMB = "thumb";

    @Autowired
    private StorageService storageService;

    @Override
    public String saveFile(String originalFilename, InputStream inputStream, CreationOptions options) throws BusinessException {
        File originalImageFile = storageService.getFile((String) options.get(CreationOptions.OPTION_SOURCE));

        File thumbFile = storageService.getFile(
                options.get(OPTION_PREFIX) + originalImageFile.getName());

        prepare(thumbFile);
        try {
            ConvertCmd cmd = new ConvertCmd();

            IMOperation op = new IMOperation();
            op.autoOrient();
            op.addImage(originalImageFile.getAbsolutePath());
            op.resize((int) options.get(OPTION_THUMB_WIDTH), (int) options.get(OPTION_THUMB_HEIGHT));
            op.addImage(thumbFile.getAbsolutePath());

            cmd.run(op);
        } catch (InterruptedException | IOException | IM4JavaException e) {
            throw new BusinessException(e);
        }

        return thumbFile.getName();
    }

    @Override
    public boolean handles(String originalFilename, CreationOptions options) {
        return originalFilename == null && TYPE_THUMB.equals(getCreationType(options));
    }
}
