package me.neboard.files.neboardfiles.creation;

import me.neboard.files.neboardfiles.exception.BusinessException;
import me.neboard.files.neboardfiles.filename.FilenameGenerationStrategy;
import me.neboard.files.neboardfiles.service.FileMetaService;
import me.neboard.files.neboardfiles.service.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

@Component
public class SimpleFileStrategy extends AbstractOptionStrategy {
    @Autowired
    private FileMetaService metaService;

    @Autowired
    private StorageService storageService;

    @Autowired
    @Qualifier("filenameGenerationStrategy")
    private FilenameGenerationStrategy filenameStrategy;

    @Override
    public String saveFile(String originalFilename, InputStream inputStream, CreationOptions options) throws BusinessException {
        String newFilename = filenameStrategy.getNewFilename(originalFilename);

        File newFile = storageService.getFile(newFilename);
        try {
            prepare(newFile);
            Files.copy(inputStream, newFile.toPath());
        } catch (IOException e) {
            throw new BusinessException(e);
        }

        return newFilename;
    }

    @Override
    public boolean handles(String originalFilename, CreationOptions options) {
        return originalFilename != null && options.isEmpty();
    }

}
