package me.neboard.files.neboardfiles.creation;

import me.neboard.files.neboardfiles.exception.BusinessException;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;

public interface FileCreationStrategy {
    String saveFile(String originalFilename, InputStream inputStream, CreationOptions options) throws BusinessException;
    boolean handles(String originalFilename, CreationOptions options);
}
