package me.neboard.files.neboardfiles.creation;

import me.neboard.files.neboardfiles.exception.BusinessException;
import me.neboard.files.neboardfiles.service.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

@Component
public class FileImportStrategy extends AbstractOptionStrategy {
    private static final String CREATION_TYPE = "import";

    @Autowired
    private StorageService storageService;

    @Override
    public String saveFile(String originalFilename, InputStream inputStream, CreationOptions options) throws BusinessException {
        File newFile = storageService.getFile(originalFilename);

        // Although we should not rely on duplicates check and import the file
        // anyway, we don't need to recreate it if it already exists
        if (!newFile.exists()) {
            try {
                prepare(newFile);
                Files.copy(inputStream, newFile.toPath());
            } catch (IOException e) {
                throw new BusinessException(e);
            }
        }

        return newFile.getName();
    }

    @Override
    public boolean handles(String originalFilename, CreationOptions options) {
        return originalFilename != null && CREATION_TYPE.equals(getCreationType(options));
    }
}
