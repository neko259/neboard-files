package me.neboard.files.neboardfiles.creation;

import java.util.HashMap;
import java.util.Map;

public class CreationOptions {
    public static final String OPTION_CREATION_TYPE = "type";
    public static final String OPTION_SOURCE = "source";

    public static final String TYPE_IMPORT = "import";
    public static final String TYPE_THUMB = "thumb";

    private Map<String, Object> options;

    public CreationOptions() {
        options = new HashMap<>();
    }

    public CreationOptions(Map<String, Object> options) {
        this.options = options;
    }

    public void add(String key, Object value) {
        options.put(key, value);
    }

    public Object get(String key) {
        return options.get(key);
    }

    public boolean isEmpty() {
        return options.isEmpty();
    }
}
