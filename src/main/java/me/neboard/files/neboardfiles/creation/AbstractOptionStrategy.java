package me.neboard.files.neboardfiles.creation;

import java.io.File;

public abstract class AbstractOptionStrategy implements FileCreationStrategy {
    protected String getCreationType(CreationOptions options) {
        return (String) options.get(CreationOptions.OPTION_CREATION_TYPE);
    }

    protected void prepare(File newFile) {
        newFile.getParentFile().mkdirs();
    }
}
