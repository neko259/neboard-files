package me.neboard.files.neboardfiles.strategy;

public interface Strategy {
    String getType();
}
