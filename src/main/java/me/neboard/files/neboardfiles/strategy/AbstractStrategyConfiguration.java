package me.neboard.files.neboardfiles.strategy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.stream.Collectors;

public abstract class AbstractStrategyConfiguration<T extends Strategy> {
    private static final Logger log = LoggerFactory.getLogger(AbstractStrategyConfiguration.class);

    @Autowired
    private Collection<T> strategies;

    public T getStrategy() {
        String strategyType = getStrategyType();
        for (T strategy : strategies) {
            if (strategyType.equals(strategy.getType())) {
                return strategy;
            }
        }
        throw new UnsupportedOperationException(
                "Strategy type " + strategyType + " is not supported. Available strategies are: "
        + String.join(", ", getAvailableStrategies()));
    }

    protected abstract String getStrategyType();

    private Collection<String> getAvailableStrategies() {
        return strategies.stream()
                .map(Strategy::getType)
                .collect(Collectors.toList());
    }

}
