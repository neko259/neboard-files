package me.neboard.files.neboardfiles.location;

import me.neboard.files.neboardfiles.strategy.Strategy;

public interface FileLocationStrategy extends Strategy {
    String getPath(String filename);
}
