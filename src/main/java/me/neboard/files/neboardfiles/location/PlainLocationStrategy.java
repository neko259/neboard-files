package me.neboard.files.neboardfiles.location;

import org.springframework.stereotype.Component;

@Component
public class PlainLocationStrategy implements FileLocationStrategy {
    private static final String TYPE = "plain";

    @Override
    public String getType() {
        return TYPE;
    }

    @Override
    public String getPath(String filename) {
        return filename;
    }
}
