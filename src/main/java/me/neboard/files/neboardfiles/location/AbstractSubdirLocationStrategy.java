package me.neboard.files.neboardfiles.location;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractSubdirLocationStrategy implements FileLocationStrategy {
    @Override
    public String getPath(String filename) {
        List<String> parts = new ArrayList<>(getDirectories(filename));
        parts.add(filename);

        return String.join(File.separator, parts);
    }

    protected abstract List<String> getDirectories(String filename);
}
