package me.neboard.files.neboardfiles.location;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class DelimitedSubDirLocationStrategy extends AbstractSubdirLocationStrategy {
    private static final String TYPE = "delimitedSubDir";

    @Value("${strategy.delimSubDir.delimiter}")
    private String delimiter;

    @Override
    protected List<String> getDirectories(String filename) {
        return Arrays.asList(filename.split(delimiter));
    }

    @Override
    public String getType() {
        return TYPE;
    }
}
