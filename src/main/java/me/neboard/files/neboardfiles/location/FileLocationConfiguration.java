package me.neboard.files.neboardfiles.location;

import me.neboard.files.neboardfiles.strategy.AbstractStrategyConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FileLocationConfiguration extends AbstractStrategyConfiguration<FileLocationStrategy> {
    @Value("${strategy.location}")
    private String strategyType;

    @Bean
    public FileLocationStrategy fileLocationStrategy() {
        return getStrategy();
    }

    @Override
    protected String getStrategyType() {
        return strategyType;
    }
}
