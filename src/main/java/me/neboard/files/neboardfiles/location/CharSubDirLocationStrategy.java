package me.neboard.files.neboardfiles.location;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CharSubDirLocationStrategy extends AbstractSubdirLocationStrategy {
    private static final String TYPE = "charSubDir";

    @Value("${strategy.charSubDir.level}")
    private int level;

    protected List<String> getDirectories(String filename) {
        List<String> parts = new ArrayList<>();

        int depth = Math.min(level, filename.length());
        char[] chars = filename.toCharArray();
        for (int i = 0; i < depth; i++) {
            parts.add(String.valueOf(chars[i]));
        }
        return parts;
    }

    @Override
    public String getType() {
        return TYPE;
    }
}
