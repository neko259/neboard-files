package me.neboard.files.neboardfiles.filename;

import me.neboard.files.neboardfiles.service.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class UuidFilenameStrategy extends FilenameWithExtensionStrategy {

    private static final String TYPE = "uuid";

    @Autowired
    private StorageService storageService;

    @Override
    public String getNewFilename(String originalFilename) {
        UUID uuid = UUID.randomUUID();

        String filename;
        String suffix = getSuffix(originalFilename);
        do {
            filename = suffix != null ? uuid + suffix : uuid.toString();
        } while (storageService.getFile(filename).exists());

        return filename;
    }

    @Override
    public String getType() {
        return TYPE;
    }

}
