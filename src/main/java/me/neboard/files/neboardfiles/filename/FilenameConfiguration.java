package me.neboard.files.neboardfiles.filename;

import me.neboard.files.neboardfiles.strategy.AbstractStrategyConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FilenameConfiguration extends AbstractStrategyConfiguration<FilenameGenerationStrategy> {
    @Value("${strategy.filename}")
    private String strategyType;

    @Bean
    public FilenameGenerationStrategy filenameGenerationStrategy() {
        return getStrategy();
    }

    @Override
    protected String getStrategyType() {
        return strategyType;
    }
}
