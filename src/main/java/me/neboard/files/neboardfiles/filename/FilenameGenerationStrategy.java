package me.neboard.files.neboardfiles.filename;

import me.neboard.files.neboardfiles.strategy.Strategy;

public interface FilenameGenerationStrategy extends Strategy {
    String getNewFilename(String originalFilename);
}
