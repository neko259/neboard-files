package me.neboard.files.neboardfiles.filename;

import org.apache.commons.lang3.StringUtils;

import java.util.regex.Pattern;

public abstract class FilenameWithExtensionStrategy implements FilenameGenerationStrategy {
    private static final String EXTENSION_DELIMITER = ".";

    private static final Pattern REGEX_EXTENSION_DELIMITER = Pattern.compile("\\" + EXTENSION_DELIMITER);
    private static final Pattern REGEX_NOT_LETTER_NUMBER = Pattern.compile("[^\\w\\d]");

    protected String getSuffix(String originalFilename) {
        return EXTENSION_DELIMITER + getExtension(originalFilename);
    }

    private String getExtension(String filename) {
        String suffix = null;
        if (!StringUtils.isBlank(filename)) {
            String[] nameParts = REGEX_EXTENSION_DELIMITER.split(filename);
            String[] extensionParts = REGEX_NOT_LETTER_NUMBER.split(nameParts[nameParts.length - 1]);
            suffix = extensionParts[0];
        }
        return suffix;
    }

}
